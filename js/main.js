let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

// Tạo menu show cái loại kính lên

var contentHTML = "";

dataGlasses.forEach((item) => {
//   console.log("item: ", item);
  contentHTML += `
    <img id="${item.id}" class="col item ${item.id}" src="${item.src}" alt="" onclick="showInfoGlass('${item.id}')"/>`;
});

document.getElementById("vglassesList").innerHTML = contentHTML;

// Tạo function hiện info từng kính 

let showInfoGlass = (id) => {
  
  document.getElementById("glassesInfo").style.display = "flex";

  let itemInfo = "";
  if (id == "G2") {
    dataGlasses.forEach((info) => {
      if (info.id === id) {
        itemInfo += `
            <img id="glass-img" class="chooseGlass-2 fixed-bottom" src="${info.virtualImg}" alt="" />
            <div class="d-flex row pl-2">
              <p class="title"> ${info.name} - ${info.brand} ( ${info.color})</p>
              <div class="row">
                <p class="price col-4"> $${info.price}</p>
                <p class="status col-4">Stocking</p>
              </div>
              <p class="desc"> ${info.description}</p>
            </div>
           `;
      }
    });
  } else if (id == "G3" || id == "G6") {
    dataGlasses.forEach((info) => {
      if (info.id === id) {
        itemInfo += `
          <img id="glass-img" class="chooseGlass-3 fixed-bottom" src="${info.virtualImg}" alt="" />
          <div class="d-flex row pl-2">
            <p class="title"> ${info.name} - ${info.brand} ( ${info.color})</p>
            <div class="row">
              <p class="price col-4"> $${info.price}</p>
              <p class="status col-4">Stocking</p>
            </div>
            <p class="desc"> ${info.description}</p>
          </div>
         `;
      }
    });
  } else {
    dataGlasses.forEach((info) => {
      if (info.id === id) {
        itemInfo += `
          <img id="glass-img" class="chooseGlass fixed-bottom" src="${info.virtualImg}" alt="" />
          <div class="d-flex row pl-2">
            <p class="title"> ${info.name} - ${info.brand} ( ${info.color})</p>
            <div class="row">
              <p class="price col-4"> $${info.price}</p>
              <p class="status col-4">Stocking</p>
            </div>
            <p class="desc"> ${info.description}</p>
          </div>
         `;
      }
    });
  }

  document.getElementById("glassesInfo").innerHTML = itemInfo;
};

// Function remove Glasses
function removeGlasses(id) {
  let type = document.getElementById("glass-img");
  if (id == true) {
    type.classList.add("glass-img");
    type.classList.remove("glass-img-2");
  }
  if (id == false) {
    type.classList.add("glass-img-2");
    type.classList.remove("glass-img");
  }
};
